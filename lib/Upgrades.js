// @flow

const Upgrade = {
  __height34: false,

  enableHeight34() {
    Upgrade.__height34 = true;
  },
};

export default Upgrade;
