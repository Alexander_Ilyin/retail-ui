var items = [
  'Grey Face',
  'Grey Space',
  'Kappa',
  'Keepo',
  'Resident Sleeper',
];

ReactDOM.render(<Autocomplete source={items} />, mountNode);
